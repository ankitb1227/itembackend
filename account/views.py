from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Item
from rest_framework.viewsets import ModelViewSet
from .serializer import ItemSerializer


class ItemManager(APIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    permission_classes = [IsAuthenticated, ]

    def post(self, request):
        serialized_data = self.serializer_class(data = request.data)
        if serialized_data.is_valid(raise_exception=True):
            serialized_data.save()
            return Response(serialized_data.data)

    def get(self):
        """It will return list of all items"""
        item = Item.objects.all()
        if not item.exists():
            return Response({"data": "Item doesn't exists."})
        serialized_data = self.serializer_class(item, many=True)
        return Response(serialized_data.data)


class ItemViewSet(ModelViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        queryset = self.queryset
        queryset = queryset.filter(user=self.request.user)
        return queryset
