from rest_framework.routers import SimpleRouter
from .views import ItemViewSet, ItemManager
from django.urls import path

router = SimpleRouter()
router.register(r'items', ItemViewSet, basename='item')
urlpatterns = [
    path('item/', ItemManager.as_view()),

]
urlpatterns += router.urls
