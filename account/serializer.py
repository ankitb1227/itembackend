from rest_framework import serializers
from .models import Item


class ItemSerializer(serializers.ModelSerializer):
    """Serializer for User creation"""

    class Meta:
        """ meta class """
        model = Item
        fields = '__all__'
