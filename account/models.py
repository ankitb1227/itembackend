from django.db import models
from django.contrib.auth.models import User


class Item(models.Model):
    """Sample model table of Item"""

    itemName = models.CharField(max_length=200)
    price = models.FloatField()
    quantity = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.itemName)
